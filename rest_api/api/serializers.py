from rest_framework import serializers
from rest_api.models import Meme

class MemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Meme
        fields = ["postLink","subreddit","title","url","id"]