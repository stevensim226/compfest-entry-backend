from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from rest_api.models import Meme
from rest_api.api.serializers import MemeSerializer

@api_view(["GET"])
def detail_meme_view(request,id):
    try:
        meme = Meme.objects.get(id=id)
    except Meme.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    serializer = MemeSerializer(meme)
    return Response(serializer.data)

@api_view(["GET"]) # Lists all saved memes ordered from the biggest id to smallest
def meme_list_view(request):
    saved_memes = Meme.objects.all().order_by("-id")
    serializer = MemeSerializer(saved_memes,many=True)
    return Response(serializer.data)

@api_view(["POST"]) # Saves meme to api
def save_meme_view(request):
    serializer = MemeSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)

@api_view(["DELETE"]) # Deletes meme from api by it's data
def delete_meme_view(request):
    search_data = {data[0]:data[1] for data in request.data.items()}
    try:
        meme = Meme.objects.get(**search_data)
    except:
        return Response(status = status.HTTP_404_NOT_FOUND)
    
    response = {}
    if meme.delete():
        response["status"] = "success"
    else:
        response["status"] = "failed"
    return Response(data=response)

@api_view(["POST"]) # Checks if meme exists in database, returns Boolean value
def check_meme_is_saved_view(request):
    search_data = {data[0]:data[1] for data in request.data.items()}
    response = {}
    try:
        Meme.objects.get(**search_data)
        response["exists"] = True
    except:
        response["exists"] = False
    return Response(data=response)
