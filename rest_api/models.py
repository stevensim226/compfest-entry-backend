from django.db import models

class Meme(models.Model):
    postLink = models.URLField()
    subreddit = models.TextField()
    title = models.TextField()
    url = models.URLField()

    def __str__(self):
        return "{} from r/{}".format(self.title,self.subreddit)